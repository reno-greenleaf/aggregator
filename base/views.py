from django.shortcuts import render
from django.views import View
from .models import Item, Source

class Items(View):
	def get(self, request):
		items = Item.objects.all()
		sources = Source.objects.all()
		return render(request, 'base/items-list.html', {'items': items, 'sources': sources})