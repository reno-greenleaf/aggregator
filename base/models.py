from django.db import models

class Source(models.Model):
	""" Where to pull items from. """
	url = models.URLField(unique=True)


class Item(models.Model):
	source = models.ForeignKey(Source, on_delete=models.CASCADE)
	date = models.DateTimeField(null=False)
	title = models.CharField(max_length=512)
	description = models.TextField()